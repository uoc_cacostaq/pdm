"""
Autor:  Carlos Acostas Quintas
Fecha:  18/03/2022

El archivo "utils_models.py" contiene funciones útiles para
el desarrollo de la solución PdM.
"""


def window_size():
    # Simulación de una semana
    return 60


def AE_threshold(feature):
    if feature == "vibration":
        return 0.2 
    if feature == "volt":
        return 0.2 
    if feature == "pressure":
        return 0.2 
    if feature == "rotate":
        return 0.2 


