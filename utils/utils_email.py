"""
Autor:  Carlos Acostas Quintas
Fecha:  07/04/2022

El archivo "utils_email.py" contiene funciones útiles para
el desarrollo de la solución PdM en cuanto a las notificaciones automáticas.
"""


import smtplib
from utils_password_address import get_password_email, get_address_email_user, get_address_email_sent
 
def email_alerta(rul):
    """Configura el email y el mensaje de envío de alerta"""
    # Configuración de email
    gmail_user = get_address_email_user()
    gmail_app_password = get_password_email()
 
    sent_from = gmail_user
    sent_to = [get_address_email_sent()]
    sent_subject = "ALERTA: Posible rotura/fallo en componente"
    sent_body = ("El algortimo de Mantenimiento preventivo ha detectado una posible rotura/fallo.\n\n"
                f"La prevision de rotura es de {round(rul, 2)} horas o {round((rul/24), 2)} dias." + "\n\n"
                "La maquina podria dejar de funcionar en los proximos 10 dias." + "\n\n"
                "Por favor, inicie una revision para evitar paradas en la produccion." + "\n\n"
                "\n"
                "Administrador Mantenimiento Predictivo\n")
 
    email_text = """\
From: %s
To: %s
Subject: %s
 
%s
""" % (sent_from, ", ".join(sent_to), sent_subject, sent_body)
 
    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
 
        server.ehlo()
        server.starttls()
        server.ehlo()
 
        server.login(gmail_user, gmail_app_password)
        server.sendmail(sent_from, sent_to, email_text)
        server.quit()
 
        print("Email de ALERTA enviado!")
 
    except Exception as exception:
        print("Error: %s!\n\n" % exception)
    return
 
 
if __name__ == "__main__":
 
    email_alerta()

 