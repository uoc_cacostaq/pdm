"""
Autor:  Carlos Acostas Quintas
Fecha:  07/04/2022

El archivo "utils_password.py" contiene funciones para
la obtención de passwords en el desarrollo de la solución PdM.
"""

# Cambio de política de seguridad
#https://myaccount.google.com/lesssecureapps?rapt=AEjHL4PxQS4vaPFOcjVd_UktaIv_ok4veKRpzIquXVleeaDofUY1Tn5Ym-USfxnnvuDz4GALz2n7YNeCy1_YLZtT0BDGfWKZfQ


def get_address_email_user():
    """Retorna la dirección de correo que se desea utilizar para el envío"""
    return "XXXXXX@uoc.edu"

def get_address_email_sent():
    """Retorna la dirección de correo que se desea utilizar para la recepción"""
    return "YYYYYY@uoc.edu"    

def get_password_email():
    """Retorna la contraseña del correo electrónico del user"""
    return "***********"