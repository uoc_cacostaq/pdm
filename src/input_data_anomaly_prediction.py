"""
Autor:  Carlos Acostas Quintas
Fecha:  18/03/2022
version ='1.0'

El archivo "input_data_anomaly_prediction.py" genera la predicción
de anomalías de los datos recibidos (input data) usando el modelo
creado anteriormente model_Autoencoder.
"""

# Importación de librerías
import tensorflow as tf
import pandas as pd
import numpy as np
import sys
import csv
from pickle import load
from os.path import exists

sys.path.append("C:\\PdM\\utils")
from utils_models import window_size, AE_threshold

def AE_input_data_prediction(scaler_path_file, AE_csv_path_file, index_df, feature_list, AE_model_path_file):
    """
    Genera las predicciones del modelo Autoencoder sobre
    los datos generados mediante simulación.

            Parametros:
                    scaler_path_file (str): Path del objeto scaler usado en el modelo Autoencoder
                    AE_csv_path_file (str): Path del archivo csv
                    index_df (str): Nombre de la característica usado como indice
                    feature_list (list): Lista ordenada de los nombres de las características
                    AE_model_path_file (str): Path del modelo Autoencoder

            Retorno:
                    Tupla con los datos a predecir, su prediccion de un modelo Autoencoder,
                    dataframe con la última fila del csv y el datetime en forma de lista.
    """ 
    # Carga del objeto scaler que se ha utilizado en la normalización
    # de datos durante la creación del model Autoencoder
    scaler = load(open(scaler_path_file, 'rb'))

    # Lectura del archivo csv filtrada por sus características para 
    # tener las mismas dimensiones del input del modelo Autoencoder
    df = pd.read_csv(AE_csv_path_file).set_index(index_df)
    #df = df[feature_list]
    df_index = [df.index[-1]]

    # Extraemos la última fila de valores del archivo csv
    # que serán los valores input con los que predecir las anomalias
    features_values = [df.values[-1].tolist()]
    df_last_row = pd.DataFrame (features_values, columns = feature_list)

    # Aplicación del scaler
    input_data = scaler.fit_transform(df)[-window_size():]

    # Resimensionamiento del input a las dimensiones aceptables por el modelo Autoencoder
    input_data = np.reshape(input_data, (1, input_data.shape[0], input_data.shape[1]))

    # Carga del modelo
    AE_model = tf.keras.models.load_model(AE_model_path_file)

    # Generación de las predicciones
    input_data_prediction = AE_model.predict(input_data)
    return input_data, input_data_prediction, df_last_row, df_index


def anomaly(pd_serie):
    """
    Retorna por cada elemento de una PD Series 0 si el elemento es False y 1 si es True
    """ 
    for indice, element in enumerate(pd_serie):
        if element == False:
            pd_serie[indice] = 0
        else:
            pd_serie[indice] = 1
    return pd_serie

def AE_input_data_anomaly(AE_prediction, feature_list, LSTM_csv_path_file, column_names_LSTM_csv):
    """
    Da formato a las predicciones del modelo Autoencoder sobre
    los datos generados mediante simulación.

            Parametros:
                    AE_prediction (tupla): Tupla con los datos a predecir y su prediccion de un modelo Autoencoder
                    feature_list (list): Lista de las caracteristicas del input

            Retorno:
                    True si el número de las filas de datos del
                    archivo csv son iguales o mayores a la ventana temporasl
                    False en caso contrario
    """ 
    # Generación del error generado por el LSTM Autoencoder.
    # Se genera tantas dimensiones como tamaño de la ventana temporal y se hace la media del error
    # El resultado es un vector de errores con tantas dimensiones como características
    AE_loss = np.mean(np.abs(AE_prediction[1]-AE_prediction[0]), axis = 1)

    # Se crea un dataframe con los errores generados por cada característica
    # El dataframe solamente tiene una fila, puesto que el input era solamente la última lectura de los sensores
    df_loss = pd.DataFrame(AE_loss, columns=[x +"_loss" for x in feature_list])

    # Damos formato a las anomalías: 0 si no es anomalía, 1 en caso contrario
    for feature in feature_list:
        df_loss['Threshold_' + feature] = AE_threshold(feature)
        df_loss['Anomaly_' + feature] = anomaly(df_loss[feature + '_loss'] > df_loss['Threshold_' + feature])
        del df_loss['Threshold_' + feature]
        df_loss[feature] = AE_prediction[2][feature]
        del df_loss[feature +"_loss"]
    
    # Generamos la lista con los nombres de las columnas que deseamos que tenga el dataframe como output final
    anomaly_feature_list = []
    for feature in feature_list:
           anomaly_feature_list.append(feature)
    for feature in feature_list:
           anomaly_feature_list.append('Anomaly_' + feature)

    df = df_loss[anomaly_feature_list]

    # Incluimos el datatime original al dataset
    original_index = AE_prediction[3]
    df["index"] = original_index
    df = df.set_index("index")

    if exists(LSTM_csv_path_file):
        pass
    else:
        header = column_names_LSTM_csv
        with open(LSTM_csv_path_file, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(header)


    df.to_csv(LSTM_csv_path_file, mode='a', index=True, header=False)
    return 





if __name__ == "__main__":

    #AE_input_data_anomaly(AE_prediction, feature_list, threshold)

    """
    scaler_path_file = "C:\\PdM\\datasets\\AE_working_dataset_Autoeconder\\scaler.pkl"
    AE_csv_path_file = "C:\\PdM\\datasets\\04_dataset_anomaly\\AE_to_csv.csv"   
    index_df ="datetime" 
    feature_list = ["volt", "rotate", "pressure", "vibration"]
    AE_model_path_file ="C:\\PdM\\models\\model_Autoencoder.h5" 
    AE_prediction = AE_input_data_prediction(scaler_path_file, AE_csv_path_file, index_df, feature_list, AE_model_path_file)
    feature_list = ["volt", "rotate", "pressure", "vibration"]
    LSTM_csv_path_file = "C:\\PdM\\datasets\\05_dataset_LSTM\\LSTM_to_csv.csv"  
    res = AE_input_data_anomaly(AE_prediction, feature_list)
    column_names_LSTM_csv = ["datetime", "volt", "rotate", "pressure", "vibration", "Anomaly_volt", "Anomaly_rotate", "Anomaly_pressure", "Anomaly_vibration"]
    """