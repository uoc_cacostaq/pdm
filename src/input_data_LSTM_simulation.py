"""
Autor:  Carlos Acostas Quintas
Fecha:  17/03/2022
version ='1.0'

El archivo "input_data_simulation.py" simula la generación de una instancia
de datos de una máquina partiendo de un archivo csv con múltiples
registros.
"""

# Importación de librerías
import pandas as pd
import json
from csv import DictWriter
import os
import sys

sys.path.append("C:\\PdM\\utils")
from utils_models import window_size


def check_rows_csv_LSTM(LSTM_csv_path_file):
    """
    Chequea que las filas del archivo csv sean 
    iguales o mayores a la ventana temporal
    utilizada en el modelo LSTM

            Parametros:
                    LSTM_csv_path_file (str): Path del archivo csv 

            Retorno:
                    True si el número de las filas de datos del
                    archivo csv son iguales o mayores a la ventana temporal
                    False en caso contrario
    """ 
    with open(LSTM_csv_path_file) as csv_file:
        rows_data_num  = sum(1 for line in csv_file) - 1
        if window_size() > rows_data_num:
            return False
        else:
            return True



if __name__ == "__main__":

    #check_rows_csv_LSTM(AE_csv_path_file)
    """
    LSTM_csv_path_file = "C:\\PdM\\datasets\\05_dataset_LSTM\\LSTM_to_csv.csv"
    check_rows_csv_LSTM(LSTM_csv_path_file)
    """








