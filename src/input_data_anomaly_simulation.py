"""
Autor:  Carlos Acostas Quintas
Fecha:  17/03/2022
version ='1.0'

El archivo "input_data_simulation.py" simula la generación de una instancia
de datos de una máquina partiendo de un archivo csv con múltiples
registros.
"""

# Importación de librerías
import pandas as pd
import json
import csv
from csv import DictWriter
import os
import time
import sys
from os.path import exists

sys.path.append("C:\\PdM\\utils")
from utils_models import window_size


def input_generation(csv_path_file, json_path_file):
    """
    Genera un archivo json con la información de la primera
    fila del archivo csv y guarda el csv sin la fila utilizada.

            Parametros:
                    csv_path_file (str): Path del archivo csv 
                    json_path_file (str): Path del json creado

            Retorno:
                    Nulo
    """
    # Lectura archivo csv y conversión a dataframe
    df = pd.read_csv(csv_path_file)

    # Seleccionamos la primera fila del dataframe
    row_input = df.iloc[:1]

    # Creamos diccionario con los datos de la primera fila del dataframe
    input_row_data = {"datetime":list(row_input["datetime"])[0],
                    "volt":list(row_input["volt"])[0],
                    "rotate":list(row_input["rotate"])[0],
                    "pressure":list(row_input["pressure"])[0],
                    "vibration":list(row_input["vibration"])[0]}

    # Borramos la fila utilizada para preparar el dataframe para el 
    # siguiente registro
    df = df.iloc[1: , :]

    # Guardamos el csv sin la fila extraída
    df.to_csv(csv_path_file, index=False)

    # Guardamos en disco el diccionario en formato json
    with open(json_path_file, 'w') as jsonfile:
        json.dump(input_row_data, jsonfile)
    
    return




def json_to_last_row_of_csv(json_path_file, AE_csv_path_file, column_names_AE_csv):
    """
    Transforma la informacion de un archivo json, la
    añade como una fila más de un archivo csv (si no existe, lo crea)
    y borra el archivo json

            Parametros:
                    json_path_file (str): Path del json 
                    AE_csv_path_file (str): Path del archivo csv
                    column_names_AE_csv (list): Lista con los nombres de las columnas

            Retorno:
                    Nulo
    """ 
    # Abrimos el archivo json y lo convertimos en un diccionario  
    with open(json_path_file) as json_file:
        json_dict = json.load(json_file)
        """
        json_dict['Anomaly_volt'] = None
        json_dict['Anomaly_rotate'] = None
        json_dict['Anomaly_pressure'] = None
        json_dict['Anomaly_vibration'] = None
        """
    field_names = list(json_dict.keys())


    if exists(AE_csv_path_file):
        pass
    else:
        header = column_names_AE_csv
        with open(AE_csv_path_file, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(header)

    with open(AE_csv_path_file, 'a+', newline='') as csv_file:
        dict_writer = DictWriter(csv_file, fieldnames=field_names)
        dict_writer.writerow(json_dict)
        print(json_dict)
    os.remove(json_path_file)

    return 


def check_rows_csv_anomaly(AE_csv_path_file):
    """
    Chequea que las filas del archivo csv sean 
    iguales o mayores a la ventana temporarl
    utilizada en el modelo Autoencoder

            Parametros:
                    AE_csv_path_file (str): Path del archivo csv 

            Retorno:
                    True si el número de las filas de datos del
                    archivo csv son iguales o mayores a la ventana temporarl
                    False en caso contrario
    """ 
    with open(AE_csv_path_file) as csv_file:
        rows_data_num  = sum(1 for line in csv_file) - 1
        if window_size() > rows_data_num:
            return False
        else:
            return True



if __name__ == "__main__":

    #input_generation(csv_path_file, json_path_file)
    """
    csv_path_file = "C:\\PdM\\datasets\\02_dataset_simulation\\dataset_simulation.csv"
    json_path_file = "C:\\PdM\\datasets\\03_json\\input_row_data.json"
    input_generation(csv_path_file, json_path_file)
    """

    #json_to_last_row_of_csv(json_path_file, csv_path_file)
    """
    json_path_file = "C:\\PdM\\datasets\\03_json\\input_row_data.json"
    AE_csv_path_file = "C:\\PdM\\datasets\\04_dataset_anomaly\\AE_to_csv.csv"
    json_to_last_row_of_csv(json_path_file, AE_csv_path_file)
    """

    #check_rows_csv_anomaly(AE_csv_path_file)
    """
    AE_csv_path_file = "C:\\PdM\\datasets\\04_dataset_anomaly\\AE_to_csv.csv"
    check_rows_csv_anomaly(AE_csv_path_file)
    """
    """
    #create_csv_input_anomaly(AE_csv_path_file, column_names)
    column_names_AE_csv = ["datetime", "volt", "rotate", "pressure", "vibration"]
    """






