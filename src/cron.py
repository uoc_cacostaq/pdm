"""
Autor:  Carlos Acostas Quintas
Fecha:  21/03/2022
version ='1.0'


El archivo "cron.py" simula el flujo de la información en 
todo el sistema.
"""


import warnings
warnings.filterwarnings('ignore')

from silence_tensorflow import silence_tensorflow
silence_tensorflow()

import pandas as pd

import time
import sys
from input_data_anomaly_simulation import (input_generation, 
                                   json_to_last_row_of_csv, 
                                   check_rows_csv_anomaly)
from input_data_anomaly_prediction import (AE_input_data_prediction, 
                                           AE_input_data_anomaly)
from input_data_LSTM_simulation import (check_rows_csv_LSTM)

from input_data_LSTM_prediction import LSTM_input_data_prediction

sys.path.append("C:\\PdM\\utils")
from utils_email import email_alerta

def cron_(email_alarm=True):
    df = pd.read_csv(csv_path_file)
    
    while df.shape[0] != 0:
        input_generation(csv_path_file, 
                         json_path_file)

        json_to_last_row_of_csv(json_path_file, 
                                AE_csv_path_file,
                                column_names_AE_csv)

        if check_rows_csv_anomaly(AE_csv_path_file):
            
            AE_prediction = AE_input_data_prediction(AE_scaler_path_file,
                                                     AE_csv_path_file, 
                                                     index_df, 
                                                     feature_list, 
                                                     AE_model_path_file)

            AE_input_data_anomaly(AE_prediction, feature_list, LSTM_csv_path_file, column_names_LSTM_csv)

            if check_rows_csv_LSTM(LSTM_csv_path_file):
                
                LSTM_prediction = LSTM_input_data_prediction(LSTM_scaler_features_path_file,
                                                             LSTM_scaler_label_path_file, 
                                                             LSTM_csv_path_file, 
                                                             index_df, feature_list, 
                                                             LSTM_model_path_file, 
                                                             PBI_csv_path_file,
                                                             column_names_PBI_csv)
                # Configuración email automático
                if (LSTM_prediction[1] < 240) and (email_alarm == True):
                    print(f"Predicción de rotura menor de {LSTM_prediction[1]} horas. Se ha enviado un email de alerta!")
                    email_alerta(LSTM_prediction[1])

                print(f"Predicción de rotura/fallo en:    {round(LSTM_prediction[1], 2)} horas o {round((LSTM_prediction[1]/24), 2)} días")
            #time.sleep(3600)








if __name__ == "__main__":

    #JSON File
    json_path_file = "C:\\PdM\\datasets\\03_json\\input_row_data.json"
    
    #CSV Files
    csv_path_file = "C:\\PdM\\datasets\\02_dataset_simulation\\dataset_simulation.csv"
    AE_csv_path_file = "C:\\PdM\\datasets\\04_dataset_anomaly\\AE_to_csv.csv"
    LSTM_csv_path_file = "C:\\PdM\\datasets\\05_dataset_LSTM\\LSTM_to_csv.csv" 
    PBI_csv_path_file = "C:\\PdM\\datasets\\06_dataset_PBI\\PBI_to_csv.csv"

    #SCALER Files
    AE_scaler_path_file = "C:\\PdM\\datasets\\AE_working_dataset_Autoeconder\\AE_scaler.pkl"
    LSTM_scaler_features_path_file = "C:\\PdM\\datasets\\LSTM_working_dataset_LSTM\\LSTM_scaler_features.pkl"
    LSTM_scaler_label_path_file = 'C:\\PdM\\datasets\\LSTM_working_dataset_LSTM\\LSTM_scaler_label.pkl'

    #MODELS Files
    AE_model_path_file ="C:\\PdM\\models\\model_Autoencoder.h5" 
    LSTM_model_path_file = "C:\\PdM\\models\\model_LSTM.h5"

    #DATA variables
    index_df ="datetime" 
    feature_list = ["volt", "rotate", "pressure", "vibration"]
    column_names_AE_csv = ["datetime", "volt", "rotate", "pressure", "vibration"]
    column_names_LSTM_csv = ["datetime", "volt", "rotate", "pressure", "vibration", "Anomaly_volt", "Anomaly_rotate", "Anomaly_pressure", "Anomaly_vibration"]
    column_names_PBI_csv = ["datetime", "volt", "rotate", "pressure", "vibration", "Anomaly_volt", "Anomaly_rotate", "Anomaly_pressure", "Anomaly_vibration", "RUL", "Cycle"]
    #MAIN
    cron_()










