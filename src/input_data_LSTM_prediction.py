"""
Autor:  Carlos Acostas Quintas
Fecha:  18/03/2022
version ='1.0'

El archivo "input_data_anomaly_prediction.py" genera la predicción
de anomalías de los datos recibidos (input data) usando el modelo
creado anteriormente model_Autoencoder.
"""

# Importación de librerías
import tensorflow as tf
import pandas as pd
import numpy as np
import sys
import csv
from os.path import exists
from pickle import load

sys.path.append("C:\\PdM\\utils")
from utils_models import window_size, AE_threshold

def LSTM_input_data_prediction(scaler_features_path_file, scaler_label_path_file, LSTM_csv_path_file, index_df, feature_list, LSTM_model_path_file, PBI_csv_path_file, column_names_PBI_csv):
    """
    Genera las predicciones del modelo LSTM sobre
    los datos generados mediante simulación.

            Parametros:
                    scaler_path_file (str): Path del objeto scaler usado en el modelo LSTM
                    LSTM_csv_path_file (str): Path del archivo csv
                    index_df (str): Nombre de la característica usado como indice
                    feature_list (list): Lista ordenada de los nombres de las características
                    LSTM_model_path_file (str): Path del modelo LSTM

            Retorno:
                    Tupla con los datos a predecir, su prediccion de un modelo Autoencoder,
                    dataframe con la última fila y datetime en forma de lista.
    """ 
    # Carga del objeto scaler que se ha utilizado en la normalización
    # de datos durante la creación del model LSTM
    scaler_features = load(open(scaler_features_path_file, 'rb'))
    scaler_label = load(open(scaler_label_path_file, 'rb'))
    # Lectura del archivo csv filtrada por sus características para 
    # tener las mismas dimensiones del input del modelo LSTM
    df = pd.read_csv(LSTM_csv_path_file).set_index(index_df)
    df_index = [df.index[-1]]

    # Extraemos la última fila de valores del archivo csv
    # que serán los valores input con los que predecir la RUL
    features_values = [df.values[-1].tolist()]
    features_anomaly = ["Anomaly_"+feature for feature in feature_list]
    feature_list = feature_list + features_anomaly
    
    df_last_row = pd.DataFrame (features_values, columns = feature_list)

    # Aplicación del scaler
    input_data = scaler_features.fit_transform(df)[-window_size():]

    # Resimensionamiento del input a las dimensiones aceptables por el modelo Autoencoder
    input_data = np.reshape(input_data, (1, input_data.shape[0], input_data.shape[1]))

    # Carga del modelo
    LSTM_model = tf.keras.models.load_model(LSTM_model_path_file)

    # Generación de las predicciones
    input_data_prediction = LSTM_model.predict(input_data)

    input_data_prediction_real = scaler_label.inverse_transform(input_data_prediction).tolist()[0][0]

    df_last_row["datetime"] = df_index
    df_last_row.set_index("datetime", inplace=True)
    df_last_row["RUL"] = scaler_label.inverse_transform(input_data_prediction).tolist()[0][0]

    if exists(PBI_csv_path_file):
        pass
    else:
        header = column_names_PBI_csv
        with open(PBI_csv_path_file, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(header)

    with open(PBI_csv_path_file) as csv_file:
        df_last_row["cycle"] = sum(1 for line in csv_file)




    df_last_row.to_csv(PBI_csv_path_file, mode='a', index=True, header=False)    


    return df_last_row, input_data_prediction_real





if __name__ == "__main__":

    #AE_input_data_anomaly(AE_prediction, feature_list, threshold)

    """
    scaler_features_path_file = "C:\\PdM\\datasets\\LSTM_working_dataset_LSTM\\LSTM_scaler_features.pkl"
    scaler_label_path_file = 'C:\\PdM\\datasets\\LSTM_working_dataset_LSTM\\LSTM_scaler_label.pkl'
    LSTM_csv_path_file = "C:\\PdM\\datasets\\05_dataset_LSTM\\LSTM_to_csv.csv"   
    index_df ="datetime" 
    feature_list = ["volt", "rotate", "pressure", "vibration", "Anomaly_volt", "Anomaly_rotate", "Anomaly_pressure", "Anomaly_vibration"]
    LSTM_model_path_file ="C:\\PdM\\models\\model_LSTM.h5" 
    a = LSTM_input_data_prediction(scaler_features_path_file, scaler_label_path_file, LSTM_csv_path_file, index_df, feature_list, LSTM_model_path_file)
    PBI_csv_path_file = "C:\\PdM\\datasets\\06_dataset_PBI\\PBI_to_csv.csv"
    column_names_PBI_csv = ["datetime", "volt", "rotate", "pressure", "vibration", "Anomaly_volt", "Anomaly_rotate", "Anomaly_pressure", "Anomaly_vibration", "RUL", "Cycle"]
    """