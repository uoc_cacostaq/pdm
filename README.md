# Proyecto Final de Máster (UOC):

## Autor y Fecha
Carlos Acosta Quintas  

Mayo 2022

## Título
**Solución integral de un mantenimiento predictivo y estimación de la vida útil de máquinas a través de la detección previa de anomalías multiclase**

## Resumen

El presente trabajo pretende llevar a cabo una simulación de un entorno real industrial en el contexto del mantenimiento predictivo.
A partir de la telemetría de unos sensores anexados a las máquinas, se realizarán para cada uno de ellos, en una primera fase del modelo, una detección de anomalías clasificatoria. Dicha información se anexará a la propia telemetría para intentar, en una segunda fase, mejorar la predicción de su vida útil. Ambas fases se realizarán mediante técnicas de aprendizaje automático usando redes neuronales profundas.
Así mismo, se compararán los resultados con el mismo modelo predictivo de la vida útil sin introducir datos sobre las anomalías, para discernir si realmente se introducen mejoras en las predicciones.
La simulación incluirá envío automático de alertas predefinidas en caso de que se traspasen ciertos umbrales, un cuadro de visualización y un estudio del coste de la posible puesta en marcha de la solución.
Este repositorio representa el proyecto en su desarrollo técnico (software) sin incluir todavía la estimación del coste (a realizar en futura entrega).

El juego de datos utilizado en esta solución es: "Microsoft Azure Predictive Maintenance", que dispone de datos de 100 máquinas sobre sus sensores (voltaje, rotación, presión y vibración) estadísticamente agrupados por su media horaria, además de sus fechas de errores, mantenimientos y roturas.

# Uso

La solución está pensada y diseñada para poder recibir telemetría real vía servidores externos que aportan las instancias en formato JSON.  
Por tanto, se podría adaptar fácilmente a un entorno real donde se necesitase un mantenimiento predictivo de alguna máquina que tuviese monitorizados sus sensores.  
Así pues, la puesta en producción de la solución sería relativamente fácil de implementar, **siempre y cuando** los modelos AutoEncoder LSTM y LSTM se hubieran adaptado a los nuevos datos mediante ajuste de su ventana temporal y sus hiperparámetros, validándolos adecuadamente.  


# Pasos Previos y arranque de la solución.


## Configuración inicial para un uso correcto de la solución.

- Se debe crear un entorno que tenga los requerimientos mostrados en el archivo **"requirements.txt"**
- Se debe descargar todo el repositorio en su formato zip.
- El proyecto (carpeta pdm) una vez descomprimido debe situarse directamente en la raíz *"C:\"* del sistema.
- En directorio *"C:\pdm\datasets\02_dataset_simulation"* debe incluirse el dataset que simula el origen de datos. Este archivo deberá llamarse **"dataset_simulation.csv"**

## Acrónimos

- **RUL**: Remaining Useful Life (Vida útil)
- **EDA**: Exploratory Data Analysis
- **AE**: Autoencoder
- **LSTM**: Long Short Term Memory

## Arranque de la solución.

- Se ejecutará directamente, dentro del entorno creado, el archivo **"cron.py"** localizado en el directorio: *"C:\pdm\src\"*


# Explicación del funcionamiento de la solución.

La solución realiza una EDA previa, crea 2 modelos (incluye experimentos) y tiene 5 partes diferenciadas:

- **E**xploración de **D**atos, **A**nálisis y visualizaciones
----------------------------------------------------------------------------------------------------------------
- Modelo **AutoEncoder LSTM**:  Predicción de anomalías sobre la telemetría recibida
- Modelo **LSTM**:     Predicción de RUL sobre la telemetría recibida y sus anomalías existentes obtenidas del AE LSTM
- Modelos experimentales:  **Baseline** y **LSTM sin Anomalías**
----------------------------------------------------------------------------------------------------------------
- Parte I:      Simulación del origen de datos de entrada
- Parte II:     Aplicación del modelo AutoEncoder LSTM para detectar anomalías
- Parte III:    Aplicación del modelo LSTM para predecir la vida útil restante (RUL)
- Parte IV:     Sistema automático de aviso por email si la RUL rebasa cierto umbral
- Parte V:      Visualización de la solución (cuadro de mandos)



# Exploración de Datos y Análisis

La exploración de de Datos, una vez descargado el repositorio a un entorno local, se puede visualizar en el archivo  
**"predictive-maintenance-exploratory-data-analysis.html"**  
localizado en *"C:\pdm\html\"*

Alternativamente, se puede descargar directamente siguiento el siguiente link:

[Link EDA](https://gitlab.com/uoc_cacostaq/pdm/-/raw/main/html/predictive-maintenance-exploratory-data-analysis.html?inline=false "EDA")

Resumen de la EDA realizada:  

- Introducción de los datos de origen
- Muestra de las sábanas de los 5 archivos CSV originales.
- Dimensiones sábanas 
- Chequeo de valores nulos en sábanas
- Chequeo de valores duplicados en sábanas
- Chequeo de falta de datos en sábanas
- Muestra de los diferentes tipos de errores, mantenimientos, tipos de rotura y edades de las máquinas
- Filtrado de máquinas por "modelo", por "edad" y por "modelo y edad"
- Visualización (gráfico de líneas) de telemetria por sensores con marcadores temporales de rotura según tipo de rotura (por colores)
- Visualización (boxplot) de telemetría por sensores según el día del mes
- Visualización (boxplot) de telemetría por sensores según el mes del año
- Visualización (histograma) de la telemetría por sensores
- Correlación (matriz de correlación) de la telemetría
- Visualización (Scatter Plot) entre Fallos y Errores por fecha para observar posibles patrones entre los fallos y errores
- Visualización de la Autocorrelación de la telemetría por sensores y lags (pasos temporales) 
- Visualización de la Autocorrelación Parcial de la telemetría por sensores y lags (pasos temporales)
- Exploración de Errores por tipo y máquina
- Exploración de Mantenimientos por tipo y máquina
- Exploración de Fallos por tipo y máquina
- Exploración de Fallos por componente y máquina  

A su vez se ha realizado una función que combina los 5 archivos CSV en uno solo mediante uniones de los juegos de datos y una tabla resumen.  
  

Una vez explorados los datos, se ha realizado una función para filtrar y generar únicamente series temporales **sin fallos**, dependiendo de su modelo, edad, máquina y componente de fallo.

La función, una vez descargado el repositorio a un entorno local, se puede visualizar en el archivo  
**"series_creation.html"**  
localizado en *"C:\pdm\html\"* 

Alternativamente, se puede descargar directamente siguiento el siguiente link:

[Link Series Generation](https://gitlab.com/uoc_cacostaq/pdm/-/raw/main/html/series_creation.html?inline=false "Data Generation")  

Esta función recoge el **input del usuario**, el cual determina el modelo de la máquina, la edad, el número de la máquina y el componente de rotura y **devuelve** uno o varios archivos CSV con la telemetría de las máquinas de los datos entre las diferentes roturas seleccionadas, es decir, **es un generador de datos de entrenamiento**.  

Resumen del Modelo AE LSTM:  

- Carga de datos
- Definición de los conjuntos de Entrenamiento y Validación del modelo
- Definición y generación del modelo AutoEncoder - LSTM
- Distribuciones de las funciones de coste
- Definición de umbrales
- Creación del archivo base para el modelo LSTM
- Guardado del modelo  

# Modelo AutoEncoder LSTM

El modelo seleccionado para detectar anomalías es un AutoEncoder LSTM.  
Al ser tipo LSTM, se basa en series temporales con una cierta ventana temporal definida anteriormente (en esta solución es 60 ciclos)

El proceso es el siguiente:
- Se escogen datos considerados normales para ser entrenados por el modelo.
- El conjunto de validación son **exactamente** los mismos datos de entrada
- El modelo trata de generar los datos de entrada, y los genera con un cierto error.
- Se estudia la distribución del error, y se determina un umbral de error a partir del cual se considera un valor anómalo.
- Si nuevos datos (datos de test) pasados por el modelo generan un error mayor al del umbral, se considerarán anómalos.

Se quiere remarcar en un modelo AutoEncoder **los datos de entrenamiento y validación son los mismos**. La finalidad es conseguir un modelo que pueda replicar la distribución de los valores normales, y en caso contrario, generar un error que discriminaría la anomalía.

![AutoEncoder LSTM](/images/AELSTM.jpg "Arquitectura AutoEncoder LSTM")


El modelo AutoEncoder LSTM, una vez descargado el repositorio a un entorno local, se puede visualizar en el archivo  
**"Autoencoder.html"**  
localizado en *"C:\pdm\html\"* 

NOTA: Para visualizar correctamente este archivo, se debe descargar también la imagen embebida y colocarla en una carpeta llamada "images" al mismo nivel que el archivo html (véase estructura del repositorio para una mejor compresión). Tanto la imagen como el archivo html se pueden descargar directamente siguiento los siguientes links:

[Imagen](https://gitlab.com/uoc_cacostaq/pdm/-/raw/main/html/pics/maquina_88.png?inline=false "Imagen")  

[AutoEncoder LSTM](https://gitlab.com/uoc_cacostaq/pdm/-/raw/main/html/Autoencoder.html?inline=false "AutoEncoder LSTM")


# Modelo LSTM

El modelo seleccionado para predecir la vida útil (RUL) es un LSTM.
Al ser tipo LSTM, se basa en series temporales con una cierta ventana temporal definida anteriormente (en esta solución es 60 ciclos)

El modelo posee una capa LSTM seguida de varias capas densas que convergen en una única neurona que representa el valor númerico normalizado de la RUL.

![LSTM](/images/LSTM.jpg "Arquitectura LSTM")


El modelo LSTM, una vez descargado el repositorio a un entorno local, se puede visualizar en el archivo  
**"LSTM.html"**  
localizado en *"C:\pdm\html\"* 

Alternativamente, se puede descargar directamente siguiento el siguiente link:

[LSTM](https://gitlab.com/uoc_cacostaq/pdm/-/raw/main/html/LSTM.html?inline=false "LSTM")


Resumen del Modelo AE LSTM:  

- Carga de datos y creación columna RUL
- Definición de los conjuntos de Entrenamiento y Validación del modelo
- Definición y generación del modelo LSTM
- Guardado del modelo 
- Comprobación visual de la Validación
- Métricas de Regresión
- Conversión del problema de regresión a un problema de clasificación
- Métricas de Clasifiación Binaria

# Modelos experimentales de la solución

Como **parte experimental** y con funciones meramente comparativas, se han desarrollado alternativamente 2 modelos adicionales:  

- Modelo básico de regresión lineal (Baseline)
- Modelo LSTM **sin** previo modelaje de las anomalías  

Se podrá comprobar observando las métricas calculadas en cada notebook que el modelo básico es muy pobre, el LSTM **sin** anomalías lo mejora, y el LSTM **con** anomalías ajusta mejor las predicciones.


El modelo Base, una vez descargado el repositorio a un entorno local, se puede visualizar en el archivo  
**"Baseline.html"**  
localizado en *"C:\pdm\html\"* 

Alternativamente, se puede descargar directamente siguiento el siguiente link:

[Baseline](https://gitlab.com/uoc_cacostaq/pdm/-/raw/main/html/Baseline.html?inline=false "Baseline")


El modelo LSTM sin anomalías, una vez descargado el repositorio a un entorno local, se puede visualizar en el archivo  
**"LSTM_without_anomalies.html"**  
localizado en *"C:\pdm\html\"* 

Alternativamente, se puede descargar directamente siguiento el siguiente link:

[LSTM sin anomalías](https://gitlab.com/uoc_cacostaq/pdm/-/raw/main/html/LSTM_without_anomalies.html?inline=false "Baseline")



# Partes de la solución

La solución se gobierna desde un único archivo **"cron.py"** localizado en *"C:\pdm\src"*, el cual actua de archivo principal (main) y orquesta todos los pasos y etapas de la solución.  
Este archivo va llamando a las diferentes funciones ubicadas en los diferentes archivos situados a lo largo del repositorio y crea los archivos CSV cuando es necesario.  



## Parte I: Simulación del origen de datos de entrada

La solución pretende simular el mantenimiento predictivo en un entorno real, por tanto, para simular cada instancia que la solución recibiría cada cierto tiempo (una instancia cada hora), se ha obtado por la generación de instancias individuales (en formato **JSON**) a partir de una base de datos ya existente (formato **CSV**).
    
Es decir, a partir de un conjunto de instancias existentes (conjunto de test), extraeremos cada una de ellas, una a una, e iremos simulando toda la tubería de procesos que debemos tener hasta llegar a predecir la RUL.  

![Semantic description of image](/images/csv_json.jpg "Origen de Datos -> Instancia Individual")

### Archivo utilizado en la Parte I

**input_data_anomaly_simulation.py**        localizado en *"C:\pdm\src"*  
- Extrae la primera fila del archivo CSV (base de datos simulada) 
- Transforma la primera fila en un archivo JSON tal que el nombre de sus columnas son las claves  y su telemetría sus valores.

Es este punto, estaríamos simulando la llegada real de una instancia con la telemetría.

- Transforma el archivo JSON a una fila CSV y la añade al archivo *"AE_to_csv.csv"* localizado en "C:\pdm\datasets\04_dataset_anomaly"
- Borra el archivo JSON
- Chequea el número de filas de *"AE_to_csv.csv"* avisando cuando el número es suficiente para aplicar el AutoEncoder LSTM y predecir anomalías.

![Semantic description of image](/images/AE_to_CSV.jpg "Telemetría")

## Parte II:     Aplicación del modelo AutoEncoder LSTM para detectar anomalías  

### Archivos utilizados en la Parte II

**input_data_anomaly_prediction.py**        localizado en *"C:\pdm\src"*

- Aplica el modelo AutoEncoder LSTM creado y **registra las anomalías multiclase** (cada clase representa un sensor) en el archivo *"LSTM_to_csv.csv"* localizado en *"C:\pdm\datasets\05_dataset_LSTM"*  
- Da formato al archivo *"LSTM_to_csv.csv"* y lo guarda obteniendo una sábana de datos con la telemetría y sus anomalías correspondientes (0=valor normal; 1=anomalía)

![Semantic description of image](/images/LSTM_to_CSV.jpg "Telemetría + Anomalías")

**utils_models.py**         localizado en *"C:\pdm\utils"*

- Aporta los umbrales de errores multiclase del modelo AutoEncoder LSTM en la solución integral para la discriminar si los valores son anómalos o no.

## Parte III:    Aplicación del modelo LSTM para predecir la vida útil restante (RUL)  

### Archivos utilizados en la Parte III

**input_data_LSTM_simulation.py**       localizado en *"C:\pdm\src"*

- Chequea el número de filas de *"LSTM_to_csv.csv"* avisando cuando el número es suficiente para aplicar el LSTM y predecir la RUL.

**input_data_LSTM_prediction.py**       localizado en *"C:\pdm\src"*

- Aplica el modelo LSTM creado a la telemetría y sus anomalías y **registra la predicción RUL** y el número de ciclos (horas) acumulado en el archivo *"PBI_to_csv.csv"* localizado en *"C:\pdm\datasets\06_dataset_PBI"* 



![Semantic description of image](/images/PBI_to_CSV.jpg "Telemetría + Anomalías + RUL + Ciclos")


## Parte IV:     Sistema automático de aviso por email si la RUL rebasa cierto umbral  

### Archivos utilizados en la Parte IV

**utils_email.py**      localizado en *"C:\pdm\utils"*

- Configura el servidor de correo y da formato al email de aviso una vez la RUL sobrepasa un umbral definido.  

**utils_password_address.py**      localizado en *"C:\pdm\utils"*

- Aporta dirección de correo de salida, de llegada y la contraseña del remitente

NOTA: Se deberá indicar los correos electrónicos y contraseña de usuario (actualmente emmascarada en el repositorio)  


En la terminal, una vez la RUL sobrepasa el umbral definido, se muestra el mensaje correspondiente:

![Semantic description of image](/images/Email_enviado_VSC.jpg "Notificación Email en terminal")

A su vez, se enviará automáticamente el correspondiente email de alerta, mostrando la descripción adecuada:

![Semantic description of image](/images/email_enviado.jpg "Envío del Email")


## Parte V:      Visualización de la solución (cuadro de mandos)  

### Archivo utilizado en la Parte V

**PdM_Report.pbix**     localizado en *"C:\pdm\PBI_report"*

El archivo es un archivo de PowerBI, una solución de Business Intelligence de Microsoft, donde se visualizará en un cuadro de mando las gráficas de las telemetrías de los sensores, sus anomalías y una gráfica lineal mostrando la evolución de la predicción de la RUL.



## POWER BI (VIDEO MUESTRA DE LA SOLUCIÓN)

A continuación se muestra un video de cómo a medida que se van generando los datos predictivos a partir de la telemetría básica, tanto las anomalías como las RUL se van mostrando visualmente en el cuadro de mando generado.  

Para visualizar el vídeo, **hacer click en la siguiente imagen**:

[![](/videos/poster_video_powerbi.jpg)](https://www.youtube.com/watch?v=q5abvG7Mp7g "PowerBI Dashboard")


## PROCESO CRON (VIDEO MUESTRA DEL PROCESO)

A continuación se muestra un video de cómo el archivo **"cron.py"** va generando los archivos JSON, y los diferentes archivos CSV.  

Se puede observar que, debido a la ventana temporal tanto en el AutoEncoder LSTM como en el LSTM, los archivos CSV tienen cierto decalaje en el tiempo y datos.  

Para visualizar el vídeo, **hacer click en la siguiente imagen**:

[![](/videos/poster_video_cron.jpg)](https://www.youtube.com/watch?v=tuSneTOkO3I&t=7s "Cron Process")




# Árbol de carpetas

**datasets**:  
- Archivos originales del juego de datos de Microsoft Azure Predictive Maintenance (*"PdM_errors.csv"*, *"PdM_failures.csv"*, *"PdM_machines.csv"*, *"PdM_maint.csv"*, *"PdM_telemetry.csv"*,)
- Combinación de los archivos originales en una sábana única de datos (*"PdM_dataset.csv"*)
- Dataset de simulación (*"dataset_simulation.csv"*)
- Input del modelo AE LSTM (*"AE_to_csv.csv"*) 
- Input del modelo LSTM (*"LSTM_to_csv.csv"*) 
- Input del archivo de PowerBI (*"PBI_to_csv.csv"*) 


**html**:  
- Archivos html de los Jupyter Notebook del repositorio
- Carpeta images que contiene imágenes auxiliares embebidas de los archivos html.


**models**:  
- Archivos con los modelos guardados (formato HDF5) usados en la solución.

**PBI_report**:  
- Archivo PowerBi que contiene el cuadro de mando

**src**:  
- Carpeta *EDA* con los notebook para realizar la EDA y archivo pkl para guardar dataframes creados.
- Carpeta *model* con los diferentes notebooks utilizados para crear los diferentes modelos de la solución (entrenamiento y generación de modelos).
- Archivo cron.py y auxiliares para el correcto desarrollo de la creación del pipeline de la solución (creación JSON y archivos CSV).

**utils**:  
- Archivos para la implementación de los correos automáticos de avisos.
- Archivos para definir la ventana temporal de la solución y errores del modelo AutoEncoder LSTM por clase/sensor

**images** y **videos**:  
- Imágenes y videos auxiliares para generar el README


## MIT License

Ver licence.txt
